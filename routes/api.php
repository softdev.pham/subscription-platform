<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\API\SubscribeController;
use App\Http\Controllers\API\WebsiteController;
use App\Http\Controllers\API\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::prefix('auth')->controller(RegisterController::class)->group(function(){
    Route::post('register', 'register');
    Route::post('login', 'login');
});
        
Route::middleware('auth:sanctum')->group( function () {
    Route::group(['prefix' => 'websites'], function () {
        //Websites
        Route::get('', [WebsiteController::class, 'index']);
        Route::post('', [WebsiteController::class, 'store']);

        //Post of Websites
        Route::get('{websiteId}/posts', [PostController::class, 'getListByWebsiteId']);
        Route::post('{websiteId}/posts', [PostController::class, 'store']);

        //Subscribe of Websites
        Route::post('{websiteId}/subscribes', [SubscribeController::class, 'store']);
    });
});

