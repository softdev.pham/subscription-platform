<?php

namespace App\Services\API\Website;

use App\Repositories\WebsiteRepository;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateWebsiteService extends BaseService
{
    private WebsiteRepository $websiteRepository;

    public function __construct(WebsiteRepository $websiteRepository)
    {
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $this->data['user_id'] = Auth::id();

        return $this->websiteRepository->create($this->data);
    }
}