<?php

namespace App\Services\API\Website;

use App\Repositories\WebsiteRepository;
use App\Services\BaseService;

class ListWebsiteService extends BaseService
{
    private WebsiteRepository $websiteRepository;

    public function __construct(WebsiteRepository $websiteRepository)
    {
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        return $this->websiteRepository->all();
    }
}