<?php

namespace App\Services\API\Subscribe;

use App\Repositories\SubscribeRepository;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class CreateSubscribeService extends BaseService
{
    private SubscribeRepository $subscribeRepository;

    public function __construct(SubscribeRepository $subscribeRepository)
    {
        $this->subscribeRepository = $subscribeRepository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $this->data['user_id'] = Auth::id();

        return $this->subscribeRepository->create($this->data);
    }
}