<?php

namespace App\Services\API\Subscribe;

use App\Repositories\SubscribeRepository;
use App\Services\BaseService;

class ListSubscribeService extends BaseService
{
    private SubscribeRepository $subscribeRepository;

    public function __construct(SubscribeRepository $subscribeRepository)
    {
        $this->subscribeRepository = $subscribeRepository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        return $this->subscribeRepository->all();
    }

    /**
     * Get list posts by websiteId
     */
    public function getByWebsiteId($websiteId)
    {
        return $this->subscribeRepository->findByField('website_id', $websiteId);
    }

    /**
     * Get list posts by websiteId with User
     */
    public function getByWebsiteIdWithUser($websiteId)
    {
        return $this->subscribeRepository->getByWebsiteIdWithUser($websiteId);
    }
}