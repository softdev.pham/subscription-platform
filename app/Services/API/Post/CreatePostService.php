<?php

namespace App\Services\API\Post;

use App\Repositories\PostRepository;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreatePostService extends BaseService
{
    private PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        $this->data['user_id'] = Auth::id();

        return $this->postRepository->create($this->data);
    }
}