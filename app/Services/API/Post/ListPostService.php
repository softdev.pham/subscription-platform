<?php

namespace App\Services\API\Post;

use App\Repositories\PostRepository;
use App\Services\BaseService;

class ListPostService extends BaseService
{
    private PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Logic to handle the data
     */
    public function handle()
    {
        return $this->postRepository->all();
    }

    /**
     * Get list posts by websiteId
     */
    public function getByWebsiteId($websiteId)
    {
        return $this->postRepository->findByField('website_id', $websiteId);
    }
}