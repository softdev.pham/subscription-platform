<?php

namespace App\Jobs;

use App\Mail\SubscribeMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class SendSubscribeEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
  
    /**
     * Create a new job instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
  
    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $detail = new SubscribeMail($this->data);
            Mail::to($this->data['email'])->queue($detail);
        } catch (\Exception $exception) {
            Log::error('SubscribeSendMail:', compact('exception'));
        }
    }
}
