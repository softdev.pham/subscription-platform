<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\Post\PostResource;
use App\Services\API\Post\ListPostService;
use App\Services\API\Post\CreatePostService;
use App\Jobs\SendSubscribeEmailJob;
use App\Services\API\Subscribe\ListSubscribeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends BaseController
{
    public function getListByWebsiteId($websiteId)
    {
        $posts = resolve(ListPostService::class)->getByWebsiteId($websiteId);

        return $this->sendResponse(PostResource::collection($posts), 'Website retrieved successfully.');
    }

    public function store(Request $request, $websiteId)
    {

        $input = $request->all();
        $input['website_id'] = $websiteId;
        $input['status'] = true;

        $validator = Validator::make($input, [
            'title' => 'required',
            'body' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $result =  resolve(CreatePostService::class)->setData($input)->handle();

        $subscribers = resolve(ListSubscribeService::class)->getByWebsiteIdWithUser($websiteId);
        //send mail to subscribers
        foreach ($subscribers as $subscriber) {
            //if email is not null then send mail to admin
            if (isset($subscriber->user->email)) {
                $data['email'] = $subscriber->user->email;
                $data['post'] = new PostResource($result);
                $this->sendMailSubscribe($data);
            }
        }

        return $this->sendResponse(new PostResource($result), 'Website created successfully.');
    }

    /**
     * Send mail when submit contact
     *
     * @param array $details
     * @return void
     */
    private function sendMailSubscribe($details)
    {
        dispatch(new SendSubscribeEmailJob($details));
    }
}
