<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Website\WebsiteResource;
use App\Services\API\Website\CreateWebsiteService;
use App\Services\API\Website\ListWebsiteService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WebsiteController extends BaseController
{
    public function index()
    {
        $websites = resolve(ListWebsiteService::class)->handle();

        return $this->sendResponse(WebsiteResource::collection($websites), 'Website retrieved successfully.');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => 'required',
            'body' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $result =  resolve(CreateWebsiteService::class)->setData($input)->handle();

        return $this->sendResponse(new WebsiteResource($result), 'Website created successfully.');
    }
}
