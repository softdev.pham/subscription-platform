<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\Subscribe\SubscribeResource;
use App\Services\API\Subscribe\CreateSubscribeService;
use Illuminate\Http\Request;

class SubscribeController extends BaseController
{
    public function store(Request $request, $websiteId)
    {
        $input = $request->all();
        $input['website_id'] = $websiteId;

        $result =  resolve(CreateSubscribeService::class)->setData($input)->handle();

        return $this->sendResponse(new SubscribeResource($result), 'Website created successfully.');
    }
}
