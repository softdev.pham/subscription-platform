<?php

namespace App\Repositories;

use App\Exceptions\RepositoryException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * @var \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    protected $model;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->resetModel();
    }

    /**
     * Get the model of repository
     *
     * @return string
     */
    abstract public function getModel();

    public function resetModel()
    {
        $instance = $this->app->make($this->getModel());

        if (! $instance instanceof Model) {
            throw RepositoryException::invalidModel();
        }

        return $this->model = $instance;
    }

    /**
     * Find record by id
     *
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id, array $columns = ['*'])
    {
        $result = $this->model->findOrFail($id, $columns);

        return $result;
    }

    /**
     * Get all data of repository
     *
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(array $columns = ['*'])
    {
        /** @var \Illuminate\Database\Eloquent\Collection */
        $result = $this->model->get($columns);

        return $result;
    }

    /**
     * Get all data of repository by field
     *
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function findByField($field, $value, array $columns = ['*'])
    {
        /** @var \Illuminate\Database\Eloquent\Collection */
        $result = $this->model->where($field, $value)->get($columns);

        return $result;
    }

    /**
     * Get all data of repository by condition
     *
     * @param array $columns
     * @return \Illuminate\Support\Collection
     */
    public function findWhereIn($field, $value, array $columns = ['*'])
    {
        $result = $this->model->whereIn($field, $value)->get($columns);

        return $result;
    }

    /**
     * Create new model
     *
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $attributes)
    {
        $result = $this->model->newInstance($attributes);
        $result->save();

        return $result;
    }

    /**
     * Update the existed model
     *
     * @param mixed $id
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $attributes)
    {
        $result = $this->model->findOrFail($id);
        $result->fill($attributes);
        $result->save();

        return $result;
    }

    /**
     * Remove the existed model
     *
     * @param mixed $id
     * @return boolean
     */
    public function delete($id)
    {
        $result = $this->model->findOrFail($id);

        return $result->delete();
    }
}