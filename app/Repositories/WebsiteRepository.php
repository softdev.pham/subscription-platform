<?php

namespace App\Repositories;

use App\Models\Website;

class WebsiteRepository extends BaseRepository
{
    /**
     * Get the model of repository
     *
     * @return string
     */
    public function getModel()
    {
        return Website::class;
    }
}