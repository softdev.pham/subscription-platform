<?php

namespace App\Repositories;

use App\Models\Subscribe;

class SubscribeRepository extends BaseRepository
{
    /**
     * Get the model of repository
     *
     * @return string
     */
    public function getModel()
    {
        return Subscribe::class;
    }

    public function getByWebsiteIdWithUser($websiteId)
    {
        return $this->model->with('user')->where('website_id', $websiteId)->get();
    }
}